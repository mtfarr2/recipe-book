//
//  UIFont+CustomFonts.m
//  RecipeBook
//
//  Created by Mark Farrell on 3/18/15.
//  Copyright (c) 2015 Mark Farrell. All rights reserved.
//

#import "UIFont+CustomFonts.h"

@implementation UIFont (CustomFonts)

+(UIFont*)titleFont{
    return [UIFont fontWithName:@"Marker Felt" size:30];
}

+(UIFont*)recipeNameFont{
    return[UIFont fontWithName: @"Marker Felt" size:14];
}

+(UIFont*)recipeDetailFont{
    return [UIFont fontWithName:@"MarkerFelt-thin" size:16];
}
@end
