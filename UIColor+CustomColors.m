//
//  UIColor+CustomColors.m
//  RecipeBook
//
//  Created by Mark Farrell on 3/18/15.
//  Copyright (c) 2015 Mark Farrell. All rights reserved.
//

#import "UIColor+CustomColors.h"

@implementation UIColor (CustomColors)

+(UIColor*)orangeHeader{
    return [UIColor orangeColor];
}

+(UIColor*)titleTextColor{
    return [UIColor whiteColor];
}

@end
