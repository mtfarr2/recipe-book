//
//  RecipeViewController.m
//  RecipeBook
//
//  Created by Mark Farrell on 1/28/15.
//  Copyright (c) 2015 Mark Farrell. All rights reserved.
//

#import "RecipeViewController.h"

@interface RecipeViewController ()

@end

@implementation RecipeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    int width = self.view.frame.size.width;
    
    UIScrollView *sv = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    sv.backgroundColor = [UIColor whiteColor];
    
    
    [self.view addSubview:sv];
    
    UIImageView *image = [[UIImageView alloc]initWithFrame:CGRectMake(20, 20, width - 40, 200)];
    NSString* string = [[_recipeDictionary objectForKey:@"recipeImage"] objectForKey:@"url"];
    NSURL *url = [NSURL URLWithString:string];
    NSData *data = [NSData dataWithContentsOfURL:url];
    image.image = [UIImage imageWithData:data];
    //image.image = [UIImage imageNamed:_recipeDictionary[@"recipeImageName"]];
    [sv addSubview:image];
    
    UILabel *title = [[UILabel alloc]initWithFrame:CGRectMake(0, 240, width, 70)];;
    title.text = _recipeDictionary[@"recipeName"];
    title.backgroundColor = [UIColor orangeHeader];
    title.font = [UIFont titleFont];
    title.textAlignment =  NSTextAlignmentCenter;
    title.textColor = [UIColor titleTextColor];
    [sv addSubview:title];
    
    UITextView *tv = [[UITextView alloc]initWithFrame: CGRectMake(20, 330, width - 40, 460)];
    tv.backgroundColor = [UIColor orangeColor];
    NSString* detailString = _recipeDictionary[@"recipeDetails"];
    tv.text = detailString;
    
    
    
    tv.font = [UIFont recipeDetailFont];
    tv.editable = NO;
    tv.scrollEnabled = NO;
    [tv sizeToFit];
    [sv addSubview:tv];
    
    
    [sv setContentSize:CGSizeMake(width, tv.frame.origin.y  + tv.frame.size.height + 30)];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
