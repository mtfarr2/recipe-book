//
//  RecipeViewController.h
//  RecipeBook
//
//  Created by Mark Farrell on 1/28/15.
//  Copyright (c) 2015 Mark Farrell. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIColor+CustomColors.h"
#import "UIFont+CustomFonts.h"

@interface RecipeViewController : UIViewController

@property (nonatomic, strong) NSDictionary* recipeDictionary;

@end
