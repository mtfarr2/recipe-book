//
//  RootLevelViewController.m
//  RecipeBook
//
//  Created by Mark Farrell on 1/28/15.
//  Copyright (c) 2015 Mark Farrell. All rights reserved.
//

#import "RootLevelViewController.h"
#import "ViewController.h"

@interface RootLevelViewController ()

@end

@implementation RootLevelViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    ViewController* vc = [ViewController new];

    UINavigationController* navController = [[UINavigationController alloc]initWithRootViewController:vc];
    [self addChildViewController:navController];
    [self.view addSubview:navController.view];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
