//
//  ViewController.m
//  RecipeBook
//
//  Created by Mark Farrell on 1/21/15.
//  Copyright (c) 2015 Mark Farrell. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    //NSUserDefaults* ud = [NSUserDefaults standardUserDefaults];
    
    
    NSURL* url = [NSURL URLWithString:@"https://api.parse.com/1/classes/Recipe"];

    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:90];
    [request addValue:@"5AUOluxNaZtBbiv7NDLCJPo2QaTlwsJF1YaDlePt" forHTTPHeaderField:@"X-Parse-Application-Id"];
    [request addValue:@"3IK6NZr28R3h9iq0X4iqiY8D0i4ZgITHzJUkqUzS" forHTTPHeaderField:@"X-Parse-REST-API-Key"];
    [request setHTTPMethod:@"GET"];

    __autoreleasing NSURLResponse  *response = nil;

    NSError *errorObj;

    NSData *data= [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&errorObj];

    NSString *responseString =   [[NSString alloc] initWithBytes:[data bytes] length:[data length] encoding:NSUTF8StringEncoding];
    
    NSLog(@"%@", responseString);
    

    array = [[NSJSONSerialization JSONObjectWithData:data options:0 error:nil] objectForKey:@"results"];

    /*if (array.count > 0){
        [ud setObject:array forKey:@"array"];
        [ud synchronize];
        
    } else {
        array = [ud objectForKey:@"array"];
    }*/
    
    UICollectionViewFlowLayout* fl = [[UICollectionViewFlowLayout alloc]init];
    
    myCollectionView = [[UICollectionView alloc]initWithFrame:self.view.bounds collectionViewLayout:fl];
    [myCollectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"cell"];
    myCollectionView.delegate = self;
    myCollectionView.dataSource = self;
    
    [self.view addSubview:myCollectionView];
    
    self.view.backgroundColor = [UIColor whiteColor];
   
    
    //int height = self.view.frame.size.height;
    //int width = self.view.frame.size.width;
    
    //NSMutableArray* arr = [[NSMutableArray alloc]initWithObjects:@"", nil];
    
    /* Dictionary
     {
        @"key":value " ",
        @"key":value " "
     }
     
     
     
    
    NSDictionary* dictionary = @{
                                    @"Key1":@"Password",
                                    @"key2":@"password",
                                    @"key3":@[
                                            @"first value",
                                            @"second value",
                                            @"third value"]
                                 };
    NSLog(@"%@", [dictionary objectForKey:@"key3"]);
     
     
    NSUserDefaults* ud = NSUserDefaults standardUserDefaults];
     

     
    
     */

    
}

-(NSInteger)collectionView :(UICollectionView *)view numberOfItemsInSection:(NSInteger)section
{
    return array.count;
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)cv cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    UICollectionViewCell *cell = [cv dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    
    cell.backgroundColor = [UIColor whiteColor];
    
    PFObject* dictionary = [array objectAtIndex:indexPath.row];
    
    UIImageView* iv = [[UIImageView alloc]initWithFrame:CGRectMake(5, 5, cell.frame.size.width-10, cell.frame.size.width-10)];
    NSString* string = [[dictionary objectForKey:@"recipeImage"] objectForKey:@"url"];
    NSURL *url = [NSURL URLWithString:string];
    NSData *data = [NSData dataWithContentsOfURL:url];
    iv.image = [UIImage imageWithData:data];
    iv.contentMode = UIViewContentModeScaleToFill;
    [cell addSubview:iv];
    
    UILabel* lbl = [[UILabel alloc]initWithFrame:CGRectMake(iv.frame.origin.x, iv.frame.origin.y + iv.frame.size.height,cell.frame.size.width, cell.frame.size.height-iv.frame.size.height)];
    lbl.font = [UIFont recipeNameFont];
    lbl.text = dictionary[@"recipeName"];
    [cell addSubview:lbl];
    
    return cell;
    
    
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    [collectionView deselectItemAtIndexPath:indexPath animated:YES];
    
    RecipeViewController* rvc = [[RecipeViewController alloc]init];
    rvc.recipeDictionary = [array objectAtIndex:indexPath.item];
    [self.navigationController pushViewController:rvc animated:YES];
    
    
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    
    return CGSizeMake(self.view.frame.size.width/2 - 15, self.view.frame.size.height/3 - 30);
}

- (UIEdgeInsets)collectionView:
(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(10, 10, 10, 10);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
