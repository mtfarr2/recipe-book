//
//  ViewController.h
//  RecipeBook
//
//  Created by Mark Farrell on 1/21/15.
//  Copyright (c) 2015 Mark Farrell. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>
{
    UITableView* myTableView;
    NSArray* array;
}
@end

