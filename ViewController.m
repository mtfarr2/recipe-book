//
//  ViewController.m
//  RecipeBook
//
//  Created by Mark Farrell on 1/21/15.
//  Copyright (c) 2015 Mark Farrell. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSString *path = [[NSBundle mainBundle] pathForResource:@"Property List" ofType:@"plist"];
    array = [[NSArray alloc]initWithContentsOfFile:path];
    
    myTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 20, self.view.frame.size.width, self.view.frame.size.height - 20) style:UITableViewStylePlain];
    myTableView.delegate = self;
    myTableView.dataSource = self;
    [self.view addSubview:myTableView];
    
    
    //int height = self.view.frame.size.height;
    //int width = self.view.frame.size.width;
    
    //NSMutableArray* arr = [[NSMutableArray alloc]initWithObjects:@"", nil];
    
    /* Dictionary
     {
        @"key":value " ",
        @"key":value " "
     }
     
     
     
    
    NSDictionary* dictionary = @{
                                    @"Key1":@"Password",
                                    @"key2":@"password",
                                    @"key3":@[
                                            @"first value",
                                            @"second value",
                                            @"third value"]
                                 };
    NSLog(@"%@", [dictionary objectForKey:@"key3"]);
     
    
     */
    
    /*UIScrollView *sv = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    sv.backgroundColor = [UIColor whiteColor];
    [sv setContentSize:CGSizeMake(width * 5, height * 1.2)];
    [self.view addSubview:sv];
    
    
    UIView *page1 = [[UIView alloc]initWithFrame:CGRectMake(0, 0, width, height * 1.2)];
    [sv addSubview:page1];
    
    
    UILabel *title1 = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, width, 70)];
    title1.text = [NSString stringWithFormat:@"Spaghetti"];
    title1.backgroundColor = [UIColor orangeColor];
    title1.font = [UIFont fontWithName:@"Marker Felt" size:30];
    title1.textAlignment =  NSTextAlignmentCenter;
    title1.textColor = [UIColor whiteColor];
    [page1 addSubview:title1];
    
    UIImageView *image1 = [[UIImageView alloc]initWithFrame:CGRectMake(20, 90, width - 40, 200)];
    image1.image = [UIImage imageNamed:@"SpaghettiPng"];
    [page1 addSubview:image1];
    
    UITextView *tv1 = [[UITextView alloc]initWithFrame: CGRectMake(20, 310, width - 40, 460)];
    tv1.backgroundColor = [UIColor orangeColor];
    tv1.text = [NSString stringWithFormat:@"What you will need: \n\n1lb Sausauge\n1 pkg thin spaghetti noodles\n1 can Hunts Meat Flavored Spaghetti Sauce\n1 Jar Ragu Garlic and Onion Spaghetti Sauce\n1 Onion diced\n2 tsp olive oil\n6 cups water\n\nNoodle Instructions:\n\nBring water and olive oil to a boil.  Add dry noodles and then return to a boil and boil for 8-9 minutes.  Drain.  DO NOT RINSE.  \n\nSauce Instructions:\n\nPlace sausage in a large skillet on medium heat.  After meat starts to brown add onion.  Once meat is completely brown add the two types of spaghetti sauce and reduce heat to medium-low and continue to heat untill hot.  Stirring occasionally.\n\n"];
    
    tv1.font = [UIFont fontWithName:@"MarkerFelt-thin" size:16];
    tv1.editable = NO;
    tv1.scrollEnabled = NO;
    [page1 addSubview:tv1];
    sv.directionalLockEnabled = YES;
    
    
    
    
    UIView *page2 = [[UIView alloc]initWithFrame:CGRectMake(width, 0, width, height * 1.2)];
    [sv addSubview:page2];
    
    
    UILabel *title2 = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, width, 70)];
    title2.text = [NSString stringWithFormat:@"Spinach Dip"];
    title2.backgroundColor = [UIColor orangeColor];
    title2.font = [UIFont fontWithName:@"Marker Felt" size:30];
    title2.textAlignment =  NSTextAlignmentCenter;
    title2.textColor = [UIColor whiteColor];
    [page2 addSubview:title2];
    
    UIImageView *image2 = [[UIImageView alloc]initWithFrame:CGRectMake(20, 90, width - 40, 200)];
    image2.image = [UIImage imageNamed:@"spinachdipPNG"];
    [page2 addSubview:image2];
    
    UITextView *tv2 = [[UITextView alloc]initWithFrame: CGRectMake(20, 310, width - 40, 460)];
    tv2.backgroundColor = [UIColor orangeColor];
    tv2.text = [NSString stringWithFormat:@"What you will need: \n\n2 8oz pkgs of cream cheese (softened)\n1/2 cup Mayonaise\n1 Cub Italian Blend Chese\n2 cloves of garlic (pressed)\n1 tsp of red cayenne pepper\n1 cup of fresh chopped spinach\n1 cup Mozzarella\n\nPreheat oven to 350 Degrees.\nLightly grease an 8x8 baking dish.\n Mix cream cheese, mayo, italian cheese, garlic, cayenne pepper together. Add in the spinach and stir. \n\nPour into the baking dish, sprinkle Mozzarella and cover with Aluminum foil. Bake for 25 minutes and remove foil and bake for and additional 5-10 minutes until cheese is golden brown. \n\nCool slightly and serve with tortilla chips or sliced baguettes"];
    
    tv2.font = [UIFont fontWithName:@"MarkerFelt-thin" size:16];
    tv2.editable = NO;
    tv2.scrollEnabled = NO;
    [page2 addSubview:tv2];
    
    
    
    UIView *page3 = [[UIView alloc]initWithFrame:CGRectMake(width * 2, 0, width, height * 1.2)];
    [sv addSubview:page3];
    
    
    UILabel *title3 = [[UILabel alloc]initWithFrame:CGRectMake(0, 240, width, 70)];
    title3.text = [NSString stringWithFormat:@"Peanut Butter Brownies"];
    title3.backgroundColor = [UIColor orangeColor];
    title3.font = [UIFont fontWithName:@"Marker Felt" size:30];
    title3.textAlignment =  NSTextAlignmentCenter;
    title3.textColor = [UIColor whiteColor];
    [page3 addSubview:title3];
    
    UIImageView *image3 = [[UIImageView alloc]initWithFrame:CGRectMake(20, 20, width - 40, 200)];
    image3.image = [UIImage imageNamed:@"Brownie"];
    [page3 addSubview:image3];
    
    UITextView *tv3 = [[UITextView alloc]initWithFrame: CGRectMake(20, 330, width - 40, 460)];
    tv3.backgroundColor = [UIColor orangeColor];
    tv3.text = [NSString stringWithFormat:@"What you will need: \n\n2 8oz pkgs of cream cheese (softened)\n1/2 cup Mayonaise\n1 Cub Italian Blend Chese\n2 cloves of garlic (pressed)\n1 tsp of red cayenne pepper\n1 cup of fresh chopped spinach\n1 cup Mozzarella\n\nPreheat oven to 350 Degrees.\nLightly grease an 8x8 baking dish.\n Mix cream cheese, mayo, italian cheese, garlic, cayenne pepper together. Add in the spinach and stir. \n\nPour into the baking dish, sprinkle Mozzarella and cover with Aluminum foil. Bake for 25 minutes and remove foil and bake for and additional 5-10 minutes until cheese is golden brown. \n\nCool slightly and serve with tortilla chips or sliced baguettes"];
    
    tv3.font = [UIFont fontWithName:@"MarkerFelt-thin" size:16];
    tv3.editable = NO;
    tv3.scrollEnabled = NO;
    [page3 addSubview:tv3];
    
    
    
    UIView *page4 = [[UIView alloc]initWithFrame:CGRectMake(width * 3, 0, width, height * 1.2)];
    [sv addSubview:page4];
    
    
    UILabel *title4 = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, width, 70)];
    title4.text = [NSString stringWithFormat:@"Spinach Dip"];
    title4.backgroundColor = [UIColor orangeColor];
    title4.font = [UIFont fontWithName:@"Marker Felt" size:30];
    title4.textAlignment =  NSTextAlignmentCenter;
    title4.textColor = [UIColor whiteColor];
    [page4 addSubview:title4];
    
    UIImageView *image4 = [[UIImageView alloc]initWithFrame:CGRectMake(20, 90, width - 40, 200)];
    image4.image = [UIImage imageNamed:@"spinachdipPNG"];
    [page4 addSubview:image4];
    
    UITextView *tv4 = [[UITextView alloc]initWithFrame: CGRectMake(20, 310, width - 40, 460)];
    tv4.backgroundColor = [UIColor orangeColor];
    tv4.text = [NSString stringWithFormat:@"What you will need: \n\n2 8oz pkgs of cream cheese (softened)\n1/2 cup Mayonaise\n1 Cub Italian Blend Chese\n2 cloves of garlic (pressed)\n1 tsp of red cayenne pepper\n1 cup of fresh chopped spinach\n1 cup Mozzarella\n\nPreheat oven to 350 Degrees.\nLightly grease an 8x8 baking dish.\n Mix cream cheese, mayo, italian cheese, garlic, cayenne pepper together. Add in the spinach and stir. \n\nPour into the baking dish, sprinkle Mozzarella and cover with Aluminum foil. Bake for 25 minutes and remove foil and bake for and additional 5-10 minutes until cheese is golden brown. \n\nCool slightly and serve with tortilla chips or sliced baguettes"];
    
    tv4.font = [UIFont fontWithName:@"MarkerFelt-thin" size:16];
    tv4.editable = NO;
    tv4.scrollEnabled = NO;
    [page4 addSubview:tv4];
    
    
    UIView *page5 = [[UIView alloc]initWithFrame:CGRectMake(width * 4, 0, width, height * 1.2)];
    [sv addSubview:page5];
    
    
    UILabel *title5 = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, width, 70)];
    title5.text = [NSString stringWithFormat:@"Spinach Dip"];
    title5.backgroundColor = [UIColor orangeColor];
    title5.font = [UIFont fontWithName:@"Marker Felt" size:30];
    title5.textAlignment =  NSTextAlignmentCenter;
    title5.textColor = [UIColor whiteColor];
    [page5 addSubview:title5];
    
    UIImageView *image5 = [[UIImageView alloc]initWithFrame:CGRectMake(20, 90, width - 40, 200)];
    image5.image = [UIImage imageNamed:@"spinachdipPNG"];
    [page5 addSubview:image5];
    
    UITextView *tv5 = [[UITextView alloc]initWithFrame: CGRectMake(20, 310, width - 40, 460)];
    tv5.backgroundColor = [UIColor orangeColor];
    tv5.text = [NSString stringWithFormat:@"What you will need: \n\n2 8oz pkgs of cream cheese (softened)\n1/2 cup Mayonaise\n1 Cub Italian Blend Chese\n2 cloves of garlic (pressed)\n1 tsp of red cayenne pepper\n1 cup of fresh chopped spinach\n1 cup Mozzarella\n\nPreheat oven to 350 Degrees.\nLightly grease an 8x8 baking dish.\n Mix cream cheese, mayo, italian cheese, garlic, cayenne pepper together. Add in the spinach and stir. \n\nPour into the baking dish, sprinkle Mozzarella and cover with Aluminum foil. Bake for 25 minutes and remove foil and bake for and additional 5-10 minutes until cheese is golden brown. \n\nCool slightly and serve with tortilla chips or sliced baguettes"];
    
    tv5.font = [UIFont fontWithName:@"MarkerFelt-thin" size:16];
    tv5.editable = NO;
    tv5.scrollEnabled = NO;
    [page5 addSubview:tv5];
     
     */
    
    
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return array.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString* identifier=@"cell";
    
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if(!cell) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    
    NSDictionary* dictionary = [array objectAtIndex:indexPath.row];
    
    UIImageView* iv = [[UIImageView alloc]initWithFrame:CGRectMake(5, 5, cell.frame.size.height-10, cell.frame.size.height-10)];
    iv.image = [UIImage imageNamed:dictionary[@"recipeImageName"]];
    [cell addSubview:iv];
    
    UILabel* lbl = [[UILabel alloc]initWithFrame:CGRectMake(iv.frame.origin.x + iv.frame.size.width + 10, 0,cell.frame.size.width - iv.frame.origin.x - iv.frame.size.width - 10, cell.frame.size.height)];
    lbl.text = dictionary[@"recipeName"];
    [cell addSubview:lbl];
    
    return cell;
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
