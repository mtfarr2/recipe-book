//
//  UIColor+CustomColors.h
//  RecipeBook
//
//  Created by Mark Farrell on 3/18/15.
//  Copyright (c) 2015 Mark Farrell. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (CustomColors)

+(UIColor*)orangeHeader;
+(UIColor*)titleTextColor;

@end
